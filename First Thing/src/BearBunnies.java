
public class BearBunnies 
{
	public static void main (String args[])
	{
		World turtleWorld = new World();
		Turtle tur1 = new Turtle(turtleWorld);
		Turtle tur2 = new Turtle(turtleWorld);
		Turtle tur3 = new Turtle(23,22,turtleWorld);
		Turtle tur4 = new Turtle(100,200,turtleWorld);
		
		tur1.forward();
		tur2.backward(30);
		tur3.turnRight();
		tur3.forward(400);
		
		Picture teddy = new Picture("bearsmall.png");
		tur1.drop(teddy);
		tur1.turn(90);
		tur1.drop(teddy);
		tur1.turn(90);
		tur1.drop(teddy);
		tur1.turn(90);
		tur1.drop(teddy);
		
	}
}
