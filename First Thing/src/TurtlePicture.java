
public class TurtlePicture 
{
	public static void main (String args[])
	{
		//creates world and turtle
		World wor = new World(600,399);
		Turtle tur = new Turtle(wor);
		
		//background
		Picture background = new Picture ("empty_field.jpg");
		wor.setPicture(background);
		
		
		//Makes the sun
		Picture sun = new Picture ("sun.png");
		tur.forward(150);
		tur.turnRight();
		tur.forward(250);
		//Creates sun
		int sunturn = 0;
		while (sunturn < 15)
		{
			tur.drop(sun);
			tur.turn(360/15);
			sunturn++;
		}
		
		//Table
		Picture table = new Picture ("table_fixed.PNG");
		table = table.scale(.35,.35);
		tur.backward(250);
		tur.backward(100);
		tur.turnRight();
		tur.forward(500);
		tur.turnRight();
		tur.turnRight();
		tur.drop(table);
		
		//Unicorn
		Picture unicorn = new Picture ("unicorn.jpg");
		unicorn = unicorn.scale(.5,.5);
		tur.forward(200);
		tur.turnLeft();
		tur.forward(50);
		tur.turnRight();
		tur.drop(unicorn);
		
		//ya man bill
		Picture bill = new Picture ("billgates.jpg");
		bill = bill.scale(.6,.6);
		tur.forward(60);
		tur.turnLeft();
//		tur.forward(15);
		tur.turn(45);
		tur.drop(bill);
		tur.turn(45);
		tur.hide();
		
		//New Turtles
		Turtle tur2 = new Turtle(wor);
		Turtle tur3 = new Turtle(wor);
		Turtle tur4 = new Turtle(wor);
		
		//New Pictures
		Picture bear2 = new Picture("bear2.png");
		Picture bunny = new Picture("bunny.png");
		Picture kiss = new Picture("kiss.png");

		//Annoying line time
		tur2.setPenWidth(4);
		tur2.forward();
		tur2.turn(90);
		tur2.forward(150);
		tur2.clearPath();
		int turturn = 0;
		while (turturn < 16)
		{
			tur2.forward(10);
			tur2.turn(360/16);
			turturn++;
			
		}
		
		//Makes bunny
		tur3.backward(150);
		tur3.drop(bunny);
		//Moves tur3 to kiss tur4
		tur3.forward(15);
		tur3.turnLeft();
		tur3.forward(90);
		
		//Makes bear
		tur4.turnLeft();
		tur4.forward(200);
		tur4.turnRight();
		tur4.backward(150);
		tur4.drop(bear2);
		//Kisses tur4
		tur4.forward(16);
		tur4.turnRight();
		tur4.forward(80);
		
		//Moves tur to place text
		tur.backward(180);
		tur.turnRight();
		tur.forward(50);
		tur.turnLeft();
		tur.drop(kiss);
		
		//Cleans up a little
		tur4.clearPath();
		tur3.clearPath();
		tur.forward(30);
		tur.clearPath();
		tur.show();
//		tur.hide();		
		
		//Makes Background
		wor.repaint();
	}
}
