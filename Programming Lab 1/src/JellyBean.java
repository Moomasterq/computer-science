//Alex Harding
//Dear diary, today I get a monitor

//Imports scanner class
import java.util.Scanner;

public class JellyBean {
	public static void main(String args [])
	{
		//Happy intro
		System.out.println("Let\' estimate the number of jelly beans in a jar");
		
		//Scanner for input
		Scanner keyboard = new Scanner(System.in);
		
		//Creates the doubles needed
		double avgLength, avgHeight;
		double jarSize;
		
		//Creates input needed
		System.out.print("Enter the average length of a jelly bean (cm): ");
		avgLength = keyboard.nextDouble();
		System.out.print("Enter the average height of a jelly bean (cm): ");
		avgHeight = keyboard.nextDouble();
		System.out.print("Enter the jar size (mL): ");
		jarSize = keyboard.nextDouble();
		
		//Final output
		System.out.println("Estimating the number of jelly beans in a jar with the following: ");
		System.out.println("    length: " + avgLength);
		System.out.println("    height: " + avgHeight);
		System.out.println("    jar size: " + jarSize);
		
		//Does math
		double jellyBean, beansInJar, beanOccupation;
		jellyBean = ( (5 * Math.PI * avgLength * (avgHeight * avgHeight) ) / 24);
		beanOccupation = (0.698 * jarSize);
		beansInJar = ( beanOccupation / jellyBean );


		//Prints Final Math
		System.out.println("The approximate amount of jelly beans in a " + jarSize + "mL jar is: " + Math.floor(beansInJar));
		
		
		
		
	}

}
