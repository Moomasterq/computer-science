public class LineStats {
       
private double X1, Y1, X2, Y2;
private double slope, distance, MidX, MidY;
private String MidP;
 
public LineStats(double x1, double y1, double x2, double y2) {
        X1 = x1;
        Y1 = y1;
        X2 = x2;
        Y2 = y2;
}
public double calcSlope(){
        slope = ((Y2 - Y1) / (X2 - X1));
        return slope;
}
public double calcDistance(){
        distance =  Math.sqrt(Math.pow((X2 - X1) , 2.0) + Math.pow((Y2 - Y1) , 2.0));
        return distance;
}
public String calcMid(){
        MidX = ((X1 + X2) / 2);
        MidY = ((Y1 + Y2) / 2);
        MidP = "(" + MidX + "," + MidY + ")";
        return MidP;
}
}