
public class BalloonTester {

	
	public static void main (String [] args){
		
		Balloon myRedOne = new Balloon();
		myRedOne.addAir(100);
		System.out.println("The radius is " + myRedOne.getRadius());  //Expected 2.8794..
		System.out.println("The volume is " + myRedOne.getVolume()); //Expected 100.0
		System.out.println("The SA is " + myRedOne.getSurfaceArea()); //Expected 104.187...
		
	}
}
