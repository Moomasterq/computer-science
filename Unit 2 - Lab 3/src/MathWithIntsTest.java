
public class MathWithIntsTest {
public static void main (String [] args){
	MathWithInts test = new MathWithInts();
	
	//Prints The factorial of 6 is 720
	System.out.println("The Factorial of  6 is " + test.getFactorial(6));

	//Prints The GCD of 12 and 20 is 4
	System.out.println("The GCD of 12 and 20 is "+ test.greatestCommonDivisor(12, 20));

	//Prints The number 18 is a prime number? false
System.out.println("The number 18 is a prime number? " + test.isPrime(18));

}
}
