
public class MathWithInts {
private int num1, num2, factorial, gcd;
private boolean prime;

public MathWithInts(){
	num1 = 0;
	num2 = 0;
	factorial = 1;
	gcd = 0;
	prime = false;
}

//Returns the factorial of a given integer
public int getFactorial (int x){
	for(int i = 1; i <= x; i++){
		factorial *= i;
	}
	return factorial;

}

//Returns the Greatest Common Divisor of two integers
public int greatestCommonDivisor(int x, int y){
	for(gcd = Math.max(x, y); gcd > 0; gcd--){
		if ((x % gcd == 0) && (y % gcd == 0))
			return gcd;
	}
	return gcd;
}

//Returns whether a number is prime.  True = prime False = not prime
public boolean isPrime(int x){
    for( int i=2 ; i < x ; i++) {
        if( x % i == 0 )
            return (prime = false);
    }
    return (prime = true);
}}
