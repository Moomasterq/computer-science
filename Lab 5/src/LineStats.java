//Alex Harding
public class LineStats {
	//Instance Variables
	private double varX1, varY1, varX2, varY2;
	private double slope, distance, midX, midY;
	private String midFinal;
	
	//Concstructor
	public LineStats(double x1, double y1, double x2, double y2) {
		varX1 = x1;
		varY1 = y1;
		varX2 = x2;
		varY2 = y2;

	}
	
	public double calcSlope(){
		slope = ((varY2 - varY1) / (varX2 - varX1));
		return slope;
	}
	
	public double calcDistance(){
		distance = Math.sqrt( Math.pow( ( varX2 - varX1 ) , 2.0) + Math.pow( (varY2 - varY1), 2.0) );
		return distance;
	}
	
	public String calcMid(){
		midX = ( ( varX1 + varX2 ) / 2 );
		midY = ( ( varY1 + varY2 ) / 2 );
		midFinal = "(" + midX + ", " + midY + ")";
		return midFinal;
	}

}
