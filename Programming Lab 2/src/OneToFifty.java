//Alex Harding
//Dear diary, I have tests tomorrow

//Imports the Scanner utility for input
import java.util.Scanner;
public class OneToFifty {
	public static void main(String args[])
	{
		//Declares 2 doubles and creates scanner
		double numberOne, numberTwo;
		Scanner keyboard = new Scanner(System.in);
		
		//User inputs
		System.out.println("Please enter 2 numbers between 1 and 50:");
		System.out.print("Number 1: ");
		numberOne = keyboard.nextDouble();
		System.out.print("Number 2: ");
		numberTwo = keyboard.nextDouble();
		
		//Tests if numbers between 1 - 50 in a less complex way than Robert's
		Boolean thinger = true;
		while (thinger){
			if (numberOne <1) {
				System.out.println("Number 1 was too small");
				System.out.println("The program will now terminate");
				System.exit(1);
			} else if (numberOne > 50) {
				System.out.println("Number 1 was too big");
				System.out.println("The program will now terminate");
				System.exit(1);
			} else if (numberTwo > 50) {
				System.out.println("Number 2 was too big");
				System.out.println("The program will now terminate");
				System.exit(1);
			} else if (numberTwo < 1) {
				System.out.println("Number 2 was too small");
				System.out.println("The program will now terminate");
				System.exit(1);
			} else {
				thinger = false;
			}}

		//Declares things for doing match
		double numberSum, numberProduct, numberAvg;
		
		//Math time
		numberSum = (numberOne + numberTwo);
		numberProduct = (numberOne * numberTwo);
		double numberMax = Math.max(numberOne, numberTwo);
		double numberMin = Math.min(numberOne, numberTwo);
		numberAvg = ( (numberOne + numberTwo) / 2 );
		
		//Sets the char thing
		char numberChar = (char) numberSum;
		
		//Final output
		System.out.println("The sum of the two numbers is: " + numberSum);
		System.out.println("The product of the two numbers is: " + numberProduct);
		System.out.println("The maximum value of the two numbers is: " + numberMax);
		System.out.println("The minimum value of the two numbers is: " + numberMin);
		System.out.println("The average of the two numbers is: " + numberAvg);
		System.out.println("The character associated with the sum of the two numbers is: " + numberChar);
		System.out.println("The end :)");
		
	}
}
