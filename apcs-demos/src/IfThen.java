import java.util.Scanner;
public class IfThen {
	public static void main(String [] args){

		int target = 32;
		String secret = "word";
		Scanner myReader = new Scanner(System.in);
		
		System.out.print("Enter a number: ");
		int input = myReader.nextInt();
		
		if(input == target) {
			System.out.println("You got it");
		}
		
		if (input > target){
			System.out.println("You went over");
		}
		
		
		System.out.print("Enter a word: ");
		String in = myReader.next();
		
		//Example of if-else
		if(secret.equals(in)) //secret.equalsIgnoreCase will ignore the case
			System.out.println("YOU GOT THE WORD");
		else
			System.out.println("You're wrong");
		

	}
	
}
