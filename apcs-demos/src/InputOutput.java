//Alex Harding
//Demonstrates input/output in java
//9-9-14

//Imports the Scanner class
import java.util.Scanner;

//Everything else
public class InputOutput {
	public static void main(String args[])
	{
		System.out.println("Gino is scary"); //basic output
		
		//User input
		Scanner alReader = new Scanner(System.in);
		System.out.print("What is your favorite number? "); //Prompt asking what they want
		int fav = alReader.nextInt(); //Reading in an integer
		System.out.print("What is your name? "); //Prompt
		String name = alReader.next(); // Reader in a string
		
		
		System.out.println(name + "'s You're favorite number is " + fav);
		
		
	}
	
}
