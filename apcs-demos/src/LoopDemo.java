import java.util.Scanner;
public class LoopDemo {
	public static void main(String args []){
		Scanner myRead = new Scanner(System.in);
		
		//ADD THE NUMBERS
		System.out.print("Enter a number: ");
		int end = myRead.nextInt();
		int total = 0;
		for (int i =1; i <= end; i++){
			total = total + i;
		}
		System.out.println("The sum is " + total);
		
		//Loop that finds letter in string
		String test = "My mom makesm muffins most days.";
		int mCount = 0;
		for (int j = 0; j < test.length(); j++){
			char get = test.charAt(j);
			if(get == 'm' || get == 'm')
				mCount++;
		}
		System.out.println("There are " + mCount);
		
		//While for el secret number
		int secret = 3;
		int guess = 0;
		while (guess != secret){
			System.out.print("Guess the number from 1-5");
			guess = myRead.nextInt();
		}
		System.out.println("YOU FOUND IT");
		
	}
}
