/*This class will find the area and perimeter
 * of a rectangle and circle.  It will show the use of 
 * the Math methods as well.
 */


public class ClassDemo {

	//Instance Variables
	private double length;
	private double width;
	private double recArea, circArea;
	private double recPer, circPer;
	private double radius;
	
	
	//Default Constructor
	public ClassDemo(){
		length = 0;
		width = 0;
		recArea = 0;
		circArea = 0;
		recPer = 0;
		circPer = 0;
		radius = 0;
	
	}
	
	//Constructor 
	public ClassDemo (int l, int w, int r){
		length = l;
		width = w;
		radius = r;
		recArea = 0;
		circArea = 0;
		recPer = 0;
		circPer = 0;
	}
	
	//Modifier Method
	public void setLength (double l){
		length = l;
	}
	//Same
	public void setWidth(double w){
		width = w;
	}
	//Same
	public void setRadius(double r){
		radius = r;
	}
	//Modifies and Accesses
	public double calculateRecArea(){
		recArea = width * length;
		return recArea;
	}
	
	public double calculateRecPer(){
		recPer = 2 * width + 2 * length;
		return recPer;
	}
	
	public double calculateCircleArea(){
		circArea = Math.pow(radius, 2) * Math.PI;
		return circArea;
	}
	
	public double calculateCirclePer(){
		circPer = 2 * radius * Math.PI;
		return circPer;
	}
}
