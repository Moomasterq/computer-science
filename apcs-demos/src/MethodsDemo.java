import java.util.Scanner;

//Alex Harding
public class MethodsDemo {

	int sum = 0;
	String name = "Lucy";
	
	//Let's write methods
	
	//No return, no parameters
	public void setName(){
		Scanner in = new Scanner(System.in);
		System.out.print("Enter your name: ");
		name = in.next();		
	}
	
	public void getName(){
		System.out.println(name);
	}
	
	//Method with return type and parameters
	public int getTheSum(int num1, int num2){
		sum = num1 + num2;
		return sum;
	}
	
	public void andAgain(){
		getName();
	}
	
}
