
//Alex Harding
//9-10-14
//New monitor comes today
//imported scanner
import java.util.Scanner;

public class VariableExample {
	public static void main(String args[])
	{
		
		Scanner myReader = new Scanner(System.in);
		
		//Declare some variables
		int age;
		String name;
		double radius, area;
		double PI = 3.14;
		
		System.out.print("Enter your age: ");
		age = myReader.nextInt();
		System.out.print("Enter your name: ");
		name = myReader.next();
		
		System.out.println(name + "'s age is " + age + ".");
		
		System.out.print("Enter a radius for a circle: ");		
		radius = myReader.nextDouble();
		area = PI * PI * radius;
		System.out.println("The area of the circle is: " + area);
		
		
		
		
		
	}
}
