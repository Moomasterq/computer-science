
//Name -
//Date -
//Class -
//Lab  -

import java.awt.Graphics;
import java.awt.Color;
import java.awt.Canvas;

class Robot extends Canvas
{
   public Robot()    //constructor method - sets up the class
   {
      setSize(800,600);
      setBackground(Color.WHITE);   	
      setVisible(true);
   }

   public void paint( Graphics window )
   {
      window.setColor(Color.BLUE);
      window.drawString("Robot LAB ", 35, 35 );
      //Calls head method
      head(window);
      upperBody(window);
      lowerBody(window);
      //call other methods
      
   }

   public void head( Graphics window )
   {
      window.setColor(Color.GRAY);
      window.fillOval(300, 100, 200, 150);
      //Eyes
      window.setColor(Color.WHITE);
      window.fillOval(350, 150, 30, 30);
      window.fillOval(425, 150, 30, 30);
      //Pupils
      window.setColor(Color.BLACK);
      window.fillOval(360, 165, 10, 10);
      window.fillOval(435, 165, 10, 10);
      //Nose
      window.setColor(Color.ORANGE);
      window.fillRect(395, 180, 15, 15);
      //Mouth
      window.setColor(Color.YELLOW);
      window.fillArc(350, 200, 100, 15, 180, 180);				
   }

   public void upperBody( Graphics window )
   {
	   //Body
	   window.setColor(Color.GRAY);
	   window.fillRect(350, 240, 100, 150);
	   //Left Arm
	   window.fillRect(250, 280, 30, 30);
	   window.fillRect(280, 290, 100, 10);
	   //Right Arm
	   window.fillRect(550, 280, 30, 30);
	   window.fillRect(450, 290, 100, 10);

	   
   }

   public void lowerBody( Graphics window )
   {
	   //Left Leg
	   window.setColor(Color.GRAY);
	   window.fillRect(355, 375, 30, 100);
	   //Right Leg
	   window.fillRect(415, 375, 30, 100);

		

   }
}