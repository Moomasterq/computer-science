//Alex Harding
public class Balloon {
	//Instance Variables
	private double radius;
	private double volume;
	private double surfaceArea;
	
	//Constructor making an empty balloon
	public Balloon() {
		radius = 0.0;
		volume = 0.0;
		surfaceArea = 0.0;
	}
	
	//Add air method
	public void addAir(double air) {
		volume = air;
	}
	
	//Finds the radius with a little bit of magic
	public double getRadius() {
		radius = ( Math.pow( ((3.0 * volume) / (4.0 * Math.PI)), (1.0/3.0)) );
		return radius;
	}
	//This one was fun to do :)
	public double getVolume() {
		return volume;
	}
	//Calculates the surface area
	public double getSurfaceArea() {
		surfaceArea = (4.0 * Math.PI * Math.pow(radius, 2.0));
		return surfaceArea;
	}
	
}
