public class Scope1 {
   private int one=4, two=3, total;	              
   public void add(){ total = one + two;  }
   public void print(){  System.out.println(total);  }

   public static void main(String args[]){
      Scope1 test = new Scope1();
      test.add();	
      test.print();
   }
}
