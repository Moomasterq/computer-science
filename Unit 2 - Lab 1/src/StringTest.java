import java.util.Scanner; //Imports Scanner for user input
public class StringTest { //no copy-pasta-rino
        public static void main(String args[]){ //In main, so it actually runs
                Scanner keyboard = new Scanner(System.in); //Creates keyboard object, from scanner class
                System.out.print("Enter an 8 character String: "); //Prompts for 8 char string
                String theInput = keyboard.nextLine(); //Takes input using the Scanner class' nextLine() method, and puts it in the string 'theInput'
                char firstLetter, lastLetter; //Creates 2 char for later use
                int length, indexOfFirstSpace; //Creates 2 int for later use
                String subString, indexString; //Creates 2 String for later use
                firstLetter = theInput.charAt(0); //Sets (char) firstLetter = theInput's to the 0th place char
                length = theInput.length(); //Sets (int) length = theInput's length
                lastLetter = theInput.charAt(length - 1); //Sets (char) lastLetter = theInput's last char (done by length - 1, which is the last char of the string)
                subString = theInput.substring(1,6); //Sets (String) subString = theInput's desired subString set what she wants on moodle
                indexOfFirstSpace = theInput.indexOf(" "); //Sets (int) indexOfFirstSpace = theInput's first appearence of ' '
                System.out.println("The first letter is " + firstLetter); //Print first letter
                System.out.println("The length is " + length); //Prints 
                System.out.println("The last letter is " + lastLetter); //Prints
                System.out.println("The substring is " + subString); //Prints
                System.out.println("The index of the first space is " + indexOfFirstSpace); //Prints
                System.out.println(""); //Prints an empty line for no reason
                String replaceIn, replaceOut, finalInput; //Creates 3 String setting up for replacing
                System.out.print("What word do you want to replace? "); //Prompts for word to be replaced
                replaceIn = keyboard.nextLine(); //Takes input
                System.out.print("What word do you want to replace " + replaceIn + " with? "); //Prompts for word that is going to replace the other one
                replaceOut = keyboard.nextLine(); //Takes input
                finalInput = theInput.replaceAll(replaceIn, replaceOut); //Does the replacing with the replaceAll() method
                System.out.println("The final output is: " + finalInput); //Prints final output
        }
}