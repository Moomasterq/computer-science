import java.util.Scanner;
//Alex Harding, not druv
public class StringThing {
	//main
	public static void main(String args[]){
		//Creates Scanner object for input
		Scanner keyboard = new Scanner(System.in);
		
		//Does stuff to make sure the length is over 8 characters long
		boolean isOver8 = true;
		String theInput = "";
		while (isOver8) {
			System.out.print("Enter a String 8 characters long: ");
			theInput = keyboard.nextLine();
			double lengther = theInput.length();
			int counter = 0;
			//Creates loop testing for the letter g
			for (int i=0; i < theInput.length() ; i++){
				if (theInput.charAt(i) == 'g' || theInput.charAt(i) == 'G' ){
					counter ++;
				}
			}
			if (lengther < 8) {
				System.out.println("Not over 8 characters long, please re-enter.");
			}
			//Forbids the letter g
			else if (counter != 0){
				//Neater response
				if (counter == 1){
					System.out.println("You have 1 too many g(s) in your input, please re-enter.");
				}
				else{
					System.out.println("You have " + counter + " too many g(s) in your input, please re-enter.");
				}
			}
			else{
				isOver8 = false;
			}
		}
		
		//Sets up variables for doing everything
		char firstLetter, lastLetter;
		int length, indexOfFirstSpace;
		String subString, indexString;
		//Does the required work
		firstLetter = theInput.charAt(0);
		length = theInput.length();
		lastLetter = theInput.charAt(length - 1);
		subString = theInput.substring(1,6);
		indexOfFirstSpace = theInput.indexOf(" ");
		
		//Tests if there is a space, if not returns n/a
		if (indexOfFirstSpace == -1){
			indexString = "n/a";
		}
		else{
			indexString = ("" + indexOfFirstSpace);
		}
		
		//Prints all of it
		System.out.println("The first letter is " + firstLetter);
		System.out.println("The length is " + length);
		System.out.println("The last letter is " + lastLetter);
		System.out.println("The substring is " + subString);
		System.out.println("The index of the first space is " + indexString);
		System.out.println(" ");
		
		//Setup for replacing
		String replaceIn, replaceOut, finalInput;
		boolean replacer = true;
		while (replacer){
			System.out.print("What word do you want to replace? ");
			replaceIn = keyboard.nextLine();
			System.out.print("What word do you want to replace " + replaceIn + " with? ");
			replaceOut = keyboard.nextLine();
			//For loop for testing g
			int counter = 0;
			for (int i=0; i < replaceOut.length() ; i++){
				if (replaceOut.charAt(i) == 'g' || replaceOut.charAt(i) == 'G'){
					counter ++;
				}
			}
			//Makes sure there are no pesky g's in it
			if (counter != 0){
				if (counter == 1){
					System.out.println("You have a g in your input, that's not allowed!");
				}
				else{
					System.out.println("You have " + counter + " too many g's in your input!");
				}
			}
			else{
				//replace() would work, but you didn't specify if you wanted only the first instance
				//or all instances replace, so I just did replaceAll() to be safe
				finalInput = theInput.replaceAll(replaceIn, replaceOut);
				System.out.println("The final output is: " + finalInput);
				replacer = false;
			}
		}
		//Closes the scanner so eclipse stops yelling at me
		keyboard.close();	
	}

}
