//hey
public class Stringerino {
	public static void main(String args[]){
		char cOne = 'o';
		char cTwo = 'l';
		String sOne = "hello there";
		String sTwo = "hallo there";

		System.out.println( sOne.indexOf('h') );		// LINE 1

		System.out.println( sOne.indexOf('7') );		// LINE 2

		System.out.println( sTwo.indexOf('a') );		// LINE 3

		System.out.println( sTwo.indexOf(cTwo) );		// LINE 4

		System.out.println( sOne.indexOf("lo") );		// LINE 5

		System.out.println( sTwo.indexOf("al") );		// LINE 6

		System.out.println( sOne.charAt(3) );			// LINE 7

		System.out.println( sOne.charAt(0) );			// LINE 8

		System.out.println( sOne.substring(3,6) );		// LINE 9

		System.out.println( sOne.substring(0,4) );		// LINE 10

		System.out.println( sOne.equals(sTwo) );		// LINE 11

		System.out.println( sOne.compareTo(sTwo) );		// LINE 12

		System.out.println( sTwo.compareTo(sOne) );		// LINE 13

		System.out.println( sTwo.compareTo("abc") );	// LINE 14

		System.out.println( sTwo.replaceAll("e","#"));	// LINE 15

		System.out.println( sTwo.replaceAll("#","*"));	// LINE 16

		System.out.println( sTwo.length() );			// LINE 17

		System.out.println( sOne.length() );			// LINE 18

		System.out.println('7');// � '0');				// LINE 19

		System.out.println('5' - 48 );				// LINE 20

//		System.out.println(sOne.charAt(20) );			// LINE 21
		
		System.out.println(sOne.toUpperCase());
		
		System.out.println(sOne);
		
		sOne = sOne.toUpperCase();
		
		System.out.println(sOne);
		


		
		
	}
	
}
