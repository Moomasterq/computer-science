import java.util.Scanner;
 
 
public class Justin {
public static void main(String [] args){
       
        String input, substring, replacestring, removedstring;
        int length, firstspace;
        char firstletter, lastletter;
       
        Scanner myReader = new Scanner (System.in);
       
        System.out.println("Insert a string");
        input = myReader.nextLine();
        length = input.length();
        lastletter = input.charAt(length-1);
        firstletter = input.charAt(0);
        substring = input.substring(1,6);
        firstspace = input.indexOf(" ");
       
        System.out.println("The Length is " + length);
        System.out.println("The Last Letter is " + lastletter);
        System.out.println("The first Letter is " + firstletter);
        System.out.println("The substring is " + substring);
        System.out.println("The index of the first space is " + firstspace);
       
        String replaceIn, replaceOut;
       
        System.out.print("Which word do you want to replace? ");
        replaceIn = myReader.nextLine();
       
        System.out.print("Which word do you want to replace? " + replaceIn + " with? ");
        replaceOut = myReader.nextLine();
       
        System.out.println("The final output is: " + input.replaceAll(replaceIn, replaceOut)); // places the replaced words
}
}