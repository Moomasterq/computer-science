
public class LineCleaner {
	  private String line;
	  private char remove;

	  public LineCleaner(String s, char rem)
	  {
		setCleaner(s,rem);
	  }

	  public void setCleaner(String s, char rem)
	  {
		  line = s;
		  remove = rem;
	  }

	  public String getCleaned()
	  {
		String cleaned=line;
		int loc = cleaned.indexOf(remove);
		String removeS = "" + remove;
		while( loc != -1 ){
			cleaned = line.replaceAll(removeS, "");
			return cleaned;
		}
		return cleaned;
	  }

	  public String toString()
	  {
		return line + " - letter to remove " + remove;
	  }
}
