


public class BankAccountTest {

	
	public static void main(String[] args) {
		
		BankAccount acct1 = new BankAccount ("Bob Smith", 72354, 102.56);
		BankAccount acct2 = new BankAccount("Patty Gomez", 54678, 759.32);
		
		System.out.println("Bob's account balance is " + acct1.getBalance());
		acct1.deposit(25.50); 
		 
		double gomezBalance = acct2.deposit(100); 
		
		System.out.println("Gomez balance after deposit is: " + gomezBalance);
		acct1.withdraw(200.00, 0.0);
		acct1.addInterest();
		acct2.addInterest();
		
		acct2.withdraw(100.50, 1.00);
		
		
		System.out.println(acct1);
		System.out.println(acct2);

	}

}
//The output will be
// Bob's account balance is 102.56 - done
//Gomez balance after deposit is: 859.32  - done
//Error: Insuffucuent funds.
// Requested: 200.00
//Available: 128.06
//72354     Bob Smith     132.5421
//54678     Patty Gomez   787.8962 
