
//Alex Harding
public class BankAccount {
//Instance Variables
	private int acctNumber; //Account number
	private double balance; //Balance of account
	private String name;  //Name of owner
	
	private final double RATE = 0.035;  //Interest Rate
	
	//Constructor - sets all the initial values of instance variables.
	public BankAccount (String owner, int account, double initial){
		name = owner;
		acctNumber = account;
		balance = initial;
	}
	
	//Determines whether number is a valid deposit and then deposits it into the account.  
	//Returns the new balance
	
	public double deposit (double amount){
		if (amount > 0.0)
			return balance += amount;
		else{
			System.out.println("Error! - You can't deposit a negative amount");
			return balance;
		}
	}
	//Validates the transaction, then withdraws the amount from the account.  Returns the new balance
	//The fee is the amount charged for the withdraw
	public double withdraw (double amount, double fee){
		if (amount > 0.0){
			if ( balance >= (amount + fee))
				return balance -= (amount + fee);
			else{
				System.out.println("Error! - Insufficient funds");
				System.out.println("Amount requested + fee: " + (amount + fee));
				System.out.println("Amount available for withdrawl: " + balance);
				return balance;
			}
		}
		else{
			System.out.println("Error! - You cannot withdraw a negative amount!");
			return balance;
		}
	}
	
	
	//Adds interest to the account and returns the new balance
	public double addInterest(){
		return balance += balance * RATE;
	}
	
	// Returns the current balance.
	public double getBalance(){
		return balance;
	}
	// Returns the account number.
	public double getAccountNumber(){
		return (double)acctNumber;
	}
	
	//Returns a one line description of the account as a string.
	public String toString(){
		return (acctNumber + "\t" + name + "\t" + balance);
	}
}
